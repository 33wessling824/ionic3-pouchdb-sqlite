import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';

/*
  Generated class for the EmployeeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EmployeeProvider {

  public pdb;
  employees;

  createPouchDB() {
    PouchDB.plugin(cordovaSqlitePlugin);
    this.pdb = new PouchDB('employee.db', {
      adapter: 'cordova-sqlite',
      iosDatabaseLocation: 'Library',
      androidDatabaseImplementation: 2
    });
    // console.log(this.pdb.adapter);
  }

  create(employee) {
    return this.pdb.post(employee);
  }

  update(employee) {
    return this.pdb.put(employee);
  }

  delete(employee) {
    return this.pdb.remove(employee);
  }


  read() {
    let pdb = this.pdb;

    function allDocs() {

      let _employees = pdb.allDocs({ include_docs: true })
        .then(docs => {
          return docs.rows;
        });

      return Promise.resolve(_employees);
    };

    // this has no functional need!
    // this.employees = pdb.changes({ live: true, since: 'now', include_docs: true })
    //   .on('change', () => {
    //     allDocs().then((emps) => {
    //       return emps;
    //     });
    //   });

    return allDocs();
  }
}
